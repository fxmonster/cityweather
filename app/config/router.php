<?php

$router = $di->getRouter();

// Define your routes here
$router->add(
    '/api/getweather/{city}',
    [
        'controller' => 'api',
        'action'     => 'getweather',
    ]
);

$router->add(
    '/api/getweather/{latitude},{longitude}',
    [
        'controller' => 'api',
        'action'     => 'getweather',
    ]
);


$router->handle();
