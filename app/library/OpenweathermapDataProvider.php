<?php

use Phalcon\Http\Client\Request as HttpClientRequest;

class OpenweathermapDataProvider
{
    public static function getCityWeather($city_name='', $latitude=0, $longitude=0)
    {
        $provider = HttpClientRequest::getProvider();
        $provider->setBaseUri('https://api.openweathermap.org/data/2.5/');
        $provider->header->set('Accept', 'application/json');

        $api_data = $provider->get(
            'weather',
            [
                'q' => $city_name,
                'lat' => $latitude,
                'lon' => $longitude,
                'units' => 'metric',
                'appid' => '1d4717953d94467bb1a206ab41bde322',
            ]
        );
        if ($api_data->header->statusCode == 200 ) {
            return $api_data->body;
        }
        return null;
    }

}