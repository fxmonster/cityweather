<?php

use Phalcon\Http\Response;
use Phalcon\Http\Client\Request as HttpClientRequest;

class ApiController extends ControllerBase
{

    public function initialize()
    {
        $this->response->setHeader('Content-Type', 'application/json');
    }

    public function indexAction()
    {
        $this->dispatcher->forward([
            'controller' => "api",
            'action' => 'citylist'
        ]);
        return;
    }

    public function cityListAction()
    {
        $cities = City::find();

        foreach($cities as $city) {
            $data[] = [
                'name'   => $city->name,
                'lastupdate' => $city->lastupdate,
                'links' => [
                    'self' => "https://".$_SERVER['HTTP_HOST']. "/api/getweather/$city->name"
                ],
            ];
        }

        $this->response->setContent(
            json_encode(['cities' => $data])
        );
        return $this->response;
    }

    public function getWeatherAction()
    {
        $city_name = $this->dispatcher->getParam('city');
        $latitude = $this->dispatcher->getParam('latitude');
        $longitude = $this->dispatcher->getParam('longitude');

        if ($city_name) {
            $city = City::findFirstByName($city_name);
        } else {
            $city = City::findFirst(
                [
                    "latitude =  $latitude",
                    "longitude =  $longitude",
                ]);
        }

        if (!$city) {
            $data = OpenweathermapDataProvider::getCityWeather($city_name, $latitude, $longitude);
            if (!$data) {
                // TODO: Add Error status code return !!!
                return $this->response->setContent(
                    'error: data prodiver return null'
                );
            }
            $array_data = json_decode($data, true);
            $city = new City();
            $city->name = $array_data['name'];
            $city->latitude = $array_data['coord']['lat'];
            $city->longitude = $array_data['coord']['lon'];
            $city->weather = $data;
            $city->save();
        }

        $this->response->setContent(
            $city->weather
        );

        return $this->response;
    }

    public function normCityAction($city_name) {
        $norm_city = Misspell::findFirstByName($city_name);

        if ($norm_city) {
            $this->response->setContent(
                $norm_city->canonical
            );
            return $this->response;
        }

        // get data from external misspells service
        $provider = HttpClientRequest::getProvider();
        $provider->setBaseUri($this->config->weather->misspellService);
        $provider->header->set('Accept', 'application/json');
        $api_data = $provider->post(
            'cityweather',
            [
                'type' => 'text',
                'text' => "город $city_name",
            ]
        );

        // bad magic - decode response array
        $array_data = json_decode($api_data->body, true);
        $result = array_filter($array_data['responses'], function($response) {
            return $response['type'] == "text";
        })[1]['text'];

        $norm_city = new Misspell(['name' => $city_name, 'canonical' => $result]);
        $norm_city->save();

        $this->response->setContent(
            $result
        );
        return $this->response;
    }


}

