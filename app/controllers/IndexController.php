<?php

use Phalcon\Http\Response;

class IndexController extends ControllerBase
{

    public function indexAction()
    {
        $this->dispatcher->forward([
            'controller' => "api",
            'action' => 'citylist'
        ]);
        return;
    }


}

